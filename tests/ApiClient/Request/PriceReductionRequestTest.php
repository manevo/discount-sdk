<?php

namespace Going\Discount\Tests\ApiClient\Request;

use Going\Discount\ApiClient\PriceReductionPayloadBuilder;
use Going\Discount\ApiClient\Request\Payload\Cart\DependencyPayload;
use Going\Discount\ApiClient\Request\Payload\PriceReductionPayload;
use Going\Discount\ApiClient\Request\Payload\User\UserPayload;
use Going\Discount\ApiClient\Request\PriceReductionRequest;
use PHPUnit\Framework\TestCase;

class PriceReductionRequestTest extends TestCase
{
    /**
     * @dataProvider getPayloadDataprovider
     */
    public function testCreateDiscount($data)
    {
        
        $payload = $this->buildPayload();
        $request = new PriceReductionRequest($payload);
        
        $result = $request->transformResponse($data);
        $this->assertTrue($result->isSuccess());
        $container = $result->getValues();
        
        $this->assertTrue($container->hasValue(4));
        $this->assertTrue($container->hasValue(2));
        $this->assertTrue($container->hasValue(1));
        $position = $container->getValue(4);
        $this->assertEquals(1000, $position->getAmountReduced());
    }
    
    
    /**
     * @return \Generator
     */
    public function getPayloadDataprovider()
    {
        yield 'standard' => [
            '{
    "code": 200,
    "message": null,
    "data": [
        {
            "tickets": [
                {
                    "amountTotal": 9000,
                    "quantity": 1,
                    "id": 2,
                    "amountReduced": 1000,
                    "positionNumber": 4
                }
            ],
            "reservations": [
                {
                    "products": [
                        {
                            "amountTotal": 35000,
                            "quantity": 1,
                            "id": 2,
                            "amountReduced": 0,
                            "positionNumber": 2
                        }
                    ],
                    "amountTotal": 10000,
                    "quantity": 4,
                    "id": 2,
                    "amountReduced": 0,
                    "positionNumber": 1
                }
            ],
            "products": []
        }
    ],
    "meta": []
}'
        ];
    }
    
    /**
     * @return PriceReductionPayload
     */
    private function buildPayload()
    {
        $builder = PriceReductionPayloadBuilder::makeDiscountPayload();
        $builder->makeTicketPosition(
            4,
            10000,
            1,
            [
                new DependencyPayload('pool',2),
                new DependencyPayload('event',2),
                new DependencyPayload('rundate',2)
            ]
        );
        
        $reservation = $builder->makeReservationsPosition(
            1,
            10000,
            4,
            [
                new DependencyPayload('pool',1),
                new DependencyPayload('event',1),
                new DependencyPayload('rundate',1)
            ]
        );
        
        $builder->addReservationProductsPosition(
            $reservation,
            2,
            35000,
            1,
            [ new DependencyPayload('product',1)]
        );
        
        return $builder->build('test11', new UserPayload('marek@zzzzzzzzzzzzzz.wp') );
    }
}
