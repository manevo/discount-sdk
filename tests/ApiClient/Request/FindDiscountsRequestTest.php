<?php

namespace Going\Discount\Tests\ApiClient\Request;

use Going\Discount\ApiClient\DiscountPayloadBuilder;
use Going\Discount\ApiClient\Request\FindDiscountsRequest;
use Going\Discount\ApiClient\Request\Payload\Discount\FindFilteredDiscountsPayload;
use PHPUnit\Framework\TestCase;

class FindDiscountsRequestTest extends TestCase
{
    /**
     * @dataProvider getPayloadDataprovider
     */
    public function testCreateDiscount($data)
    {
        
        $payload = $this->buildPayload();
        $request = new FindDiscountsRequest($payload);
        
        $result = $request->transformResponse($data);
        $this->assertTrue($result->isSuccess());
        $result = $result->getValues();
        $this->assertEquals(1,$result->getPage());
        $this->assertEquals(2,$result->getTotal());
        $this->assertEquals(20,$result->getSize());
        $discounts = $result->getResults();
        
        $this->assertCount(2,$discounts);
        $this->assertCount(2,$discounts[0]->getDependencies());
    }
    
    
    /**
     * @return \Generator
     */
    public function getPayloadDataprovider()
    {
        yield 'standard' => [
            '{
    "code": 201,
    "message": null,
    "data": [
        {
            "id": 5,
            "type": "code",
            "code": "zzBH4QQX0",
            "value": 50,
            "valuePercentage": null,
            "startDate": "2021-11-04T07:59:56+01:00",
            "expireDate": null,
            "rule": "RRULE:COUNT=1;FREQ=DAILY;",
            "createdAt": "2021-11-04T07:59:56+01:00",
            "updatedAt": "2021-11-04T07:59:56+01:00",
            "dependencies": [
                {
                    "type": "pool",
                    "externalId": 1
                },
                {
                    "type": "pool",
                    "externalId": 2
                }
            ],
            "itemDependencies": [],
            "itemLimit": null,
            "partnerId": null,
            "multiDiscount": null,
            "multiSize": null,
            "complimentary": false
        },
        {
            "id": 6,
            "type": "code",
            "code": "zzMD9LOC2",
            "value": 50,
            "valuePercentage": null,
            "startDate": "2021-11-04T07:59:56+01:00",
            "expireDate": null,
            "rule": "RRULE:COUNT=1;FREQ=DAILY;",
            "createdAt": "2021-11-04T07:59:56+01:00",
            "updatedAt": "2021-11-04T07:59:56+01:00",
            "dependencies": [
                {
                    "type": "pool",
                    "externalId": 1
                },
                {
                    "type": "pool",
                    "externalId": 2
                }
            ],
            "itemDependencies": [],
            "itemLimit": null,
            "partnerId": null,
            "multiDiscount": null,
            "multiSize": null,
            "complimentary": false
        }
    ],
    "meta": {
        "size": 20,
        "page": 1,
        "total": 2
    }
}'
        ];
    }
    
    /**
     * @return FindFilteredDiscountsPayload
     */
    private function buildPayload()
    {
        $builder = new DiscountPayloadBuilder();
        return $builder->makeGetDiscountsFilteredPayload();
    }
}
