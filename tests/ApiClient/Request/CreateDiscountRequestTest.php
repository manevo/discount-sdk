<?php

namespace Going\Discount\Tests\ApiClient\Request;

use Going\Discount\ApiClient\DiscountPayloadBuilder;
use Going\Discount\ApiClient\Request\CreateDiscountRequest;
use Going\Discount\ApiClient\Request\Payload\Discount\CreateDiscountPayload;
use PHPUnit\Framework\TestCase;

class CreateDiscountRequestTest extends TestCase
{
    /**
     * @dataProvider getPayloadDataprovider
     */
    public function testCreateDiscount($data)
    {
        
        $payload = $this->buildPayload();
        $request = new CreateDiscountRequest($payload);
        
        $result = $request->transformResponse($data);
        $this->assertTrue($result->isSuccess());
        $result = $result->getValues();
        $discount = $result->getSingleResult();
        
        $this->assertNotNull($discount);
        $this->assertCount(2,$discount->getDependencies());
        $this->assertEquals('test', $discount->getCode());
    }
    
    
    /**
     * @return \Generator
     */
    public function getPayloadDataprovider()
    {
        yield 'standard' => [
            '{
    "code": 201,
    "message": null,
    "data": [
        {
            "id": 4,
            "type": "code",
            "code": "test",
            "value": 50,
            "valuePercentage": null,
            "startDate": "2021-11-03T11:46:00+01:00",
            "expireDate": null,
            "rule": "RRULE:COUNT=1;FREQ=DAILY;",
            "createdAt": "2021-11-03T11:46:00+01:00",
            "updatedAt": "2021-11-03T11:46:00+01:00",
            "dependencies": [
                {
                    "type": "pool",
                    "externalId": 1
                },
                {
                    "type": "pool",
                    "externalId": 2
                }
            ],
            "itemDependencies": [],
            "itemLimit": null,
            "partnerId": null,
            "multiDiscount": null,
            "multiSize": null,
            "complimentary": false
        }
    ],
    "meta": []
}'
        ];
    }
    
    /**
     * @return CreateDiscountPayload
     */
    private function buildPayload()
    {
        $builder = new DiscountPayloadBuilder();
        $payload = $builder->makeCreateDiscountPayload(
            'test'
        );
        $builder->addDiscountPayloadDependency($payload, 'pool' , [1,2]);
        
        return $payload;
    }
}
