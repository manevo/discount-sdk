<?php

namespace Going\Discount\Tests\ApiClient\Request;

use Going\Discount\ApiClient\Request\DeleteDiscountRequest;
use PHPUnit\Framework\TestCase;

class DeleteDiscountRequestTest extends TestCase
{
    /**
     * @dataProvider getPayloadDataprovider
     */
    public function testCreateDiscount($data)
    {
        
        $request = new DeleteDiscountRequest(1);
        
        $result = $request->transformResponse($data);
        $this->assertTrue($result->isSuccess());
        $result = $result->getValues();
        $this->assertNull($result);
    }
    
    
    /**
     * @return \Generator
     */
    public function getPayloadDataprovider()
    {
        yield 'standard' => [
            '{
    "code": 200,
    "message": "ok",
    "data": [],
    "meta": []
}'
        ];
    }
    
}
