<?php

namespace Going\Discount\Util;

use JsonSerializable;

trait JsonTrait
{
    public static function toJson (JsonSerializable $element)
    {
        return $element->jsonSerialize();
    }
}