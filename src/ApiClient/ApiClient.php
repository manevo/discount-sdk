<?php

namespace Going\Discount\ApiClient;

use Exception;
use Going\Discount\ApiClient\Exception\DiscountApiRequestException;
use Going\Discount\ApiClient\Request\PriceReductionRequestInterface;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use function json_encode;

abstract class ApiClient
{
    /**
     * @var string
     */
    private $apiKey;
    
    /**
     * @var string
     */
    private $baseApi;
    
    /**
     * @var ClientInterface
     */
    private $guzzle;
    
    /**
     * @param string|null $apiKey
     * @param string $baseApi
     */
    public function __construct($baseApi, $apiKey = null)
    {
        $this->apiKey = $apiKey;
        $this->baseApi = $baseApi;
    }
    
    /**
     * @throws DiscountApiRequestException
     * @throws GuzzleException
     */
    protected function request(PriceReductionRequestInterface $request)
    {
        $callRequest  = $this->prepareRequest($request->getReqestType(), $request->getEndpoint(), $request->getPayload());
        
        return $request->transformResponse($this->performRequest($callRequest));
    }
    
    /**
     * @param string $method
     * @param string $endpoint
     * @param array $payload
     * @return Request
     * @noinspection CallableParameterUseCaseInTypeContextInspection
     */
    private function prepareRequest($method, $endpoint, array $payload = [])
    {
        $payload = json_encode($payload);
        $headers = $this->getHeaders();
        return new Request(
            $method,
            $this->baseApi.$endpoint,
            $headers,
            $payload
        );
    }
    
    /**
     * @param Request $request
     * @throws GuzzleException|DiscountApiRequestException
     * @return string
     */
    private function performRequest(Request $request)
    {
        try {
            if ($this->guzzle === null) {
                $this->guzzle = new Client();
            }
            $response = $this->guzzle->send($request);
        } catch (ClientException $e) {
            if (in_array($e->getCode(),[404,409], true) && $e->getResponse() !== null) {
                return $e->getResponse()->getBody()->getContents();
            }
            $this->throwError($e);
        } catch (Exception $e) {
            $this->throwError($e);
        }
        return $response->getBody()->getContents();
    }
    
    /**
     * @return array
     */
    private function getHeaders()
    {
        return [
            'accept' => 'application/json',
            'content-type' => 'application/json',
            'cache-control' => 'no-cache',
            'auth-token' => $this->apiKey ?: ''
        ];
    }
    
    /**
     * @param Exception $e
     * @throws DiscountApiRequestException
     */
    private function throwError(Exception $e)
    {
        throw new DiscountApiRequestException(sprintf('There was a problem performing this request: %s', $e->getMessage()));
    }
}
