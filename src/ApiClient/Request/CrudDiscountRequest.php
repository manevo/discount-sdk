<?php

namespace Going\Discount\ApiClient\Request;

use Going\Discount\ApiClient\Exception\DiscountApiRequestException;
use Going\Discount\ApiClient\Response\DiscountContainer;
use Going\Discount\ApiClient\Response\VO\DiscountDependencyResponseElement;
use Going\Discount\ApiClient\Response\VO\DiscountResponseElement;

abstract class CrudDiscountRequest
{
    const BASE_RESPONSE_STRUCTURE =
        [
            'id',
            'type',
            'code',
            'value',
            'valuePercentage',
            'startDate',
            'expireDate',
            'dependencies',
            'itemLimit',
            'partnerId',
            'multiDiscount',
            'multiSize',
            'complimentary'
        ];
    const DEPENDENCIES_RESPONSE_STRUCTURE = ['type', 'externalId'];
    
    /**
     * @var string
     */
    protected $error = '';
    /**
     * @var DiscountContainer|null
     */
    protected $container;
    /**
     * @var bool
     */
    protected $success = false;
    /**
     * @var array
     */
    protected $response = [];
    
    /**
     * @var array
     */
    protected $payload = [];
    
    
    /**
     * @param string $rawResponse
     * @throws DiscountApiRequestException
     * @return self
     */
    public function transformResponse($rawResponse)
    {
        $response = \json_decode($rawResponse, true);
        
        if (empty($response) || !is_array($response) || !isset($response['data'])) {
            throw new DiscountApiRequestException('No valid api response ' . (string) $rawResponse);
        }
        
        if (isset($response['code'], $response['message']) && ($response['code'] === 404 || $response['code'] === 409)) {
            $this->success = false;
            $this->error = $response['message'];
            
            return $this;
        }
        
        if (empty($response['data'][0])) {
            throw new DiscountApiRequestException('Empty api response ' . (string) $rawResponse);
        }
        
        $this->examineStructure($response['data'][0]);
        $this->success = true;
        $this->response = $response['data'][0];
        
        return $this;
    }
    
    /**
     * @throws DiscountApiRequestException
     * @return void
     */
    private function examineStructure(array $response)
    {
        foreach (self::BASE_RESPONSE_STRUCTURE as $element) {
            if (!\array_key_exists($element, $response)) {
                throw new DiscountApiRequestException('No valid api response on main structure , missing : ' . $element);
            }
        }
        if (!empty($response['dependencies'])) {
            foreach ($response['dependencies'] as $dependency) {
                foreach (self::DEPENDENCIES_RESPONSE_STRUCTURE as $item) {
                    if (!\array_key_exists($item, $dependency)) {
                        throw new DiscountApiRequestException('No valid api response on dependency structure , missing : ' . $item);
                    }
                }
                
            }
        }
    }
    
    /**
     * @return array
     */
    public function getResponse()
    {
        return $this->response;
    }
    
    /**
     * @return array
     */
    public function getPayload()
    {
        return $this->payload;
    }
    
    /**
     * @throws DiscountApiRequestException
     * @throws \Exception
     * @return DiscountContainer
     */
    public function getValues()
    {
        if ($this->container !== null) {
            return $this->container;
        }
        
        if (empty($this->response)) {
            throw new DiscountApiRequestException('Empty response');
        }
        $this->container = new DiscountContainer();
        
        $result = new DiscountResponseElement(
            $this->response['id'],
            $this->response['type'],
            $this->response['code'],
            $this->response['value'],
            $this->response['valuePercentage'],
            !empty($this->response['startDate']) ? new \DateTime($this->response['startDate']) : null,
            !empty($this->response['expireDate']) ? new \DateTime($this->response['expireDate']) : null,
            !empty($this->response['dependencies']) && is_array($this->response['dependencies']) ? \array_map(static function ($element) {
                return new DiscountDependencyResponseElement($element['type'], $element['externalId']);
            }, $this->response['dependencies']) : [],
            $this->response['itemLimit'],
            $this->response['partnerId'],
            $this->response['multiDiscount'],
            $this->response['multiSize'],
            (bool) $this->response['complimentary']
        );
        
        $this->container->addResult($result);
        
        return $this->container;
    }
    
    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }
}