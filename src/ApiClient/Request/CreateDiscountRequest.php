<?php

namespace Going\Discount\ApiClient\Request;

use Going\Discount\ApiClient\Request\Payload\Discount\CreateDiscountPayload;

class CreateDiscountRequest extends CrudDiscountRequest implements PriceReductionRequestInterface
{
    const ENDPOINT = '/api/v1/discount/create';
    
    /**
     * @param CreateDiscountPayload $payload
     */
    public function __construct(
        CreateDiscountPayload $payload
    ) {
        $this->payload = $payload->jsonSerialize();
    }
    
    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }
    
    /**
     * @return string
     */
    public function getEndpoint()
    {
        return self::ENDPOINT;
    }
    
    /**
     * @return string
     */
    public function getReqestType()
    {
        return 'POST';
    }

}