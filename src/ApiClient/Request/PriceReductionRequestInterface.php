<?php

namespace Going\Discount\ApiClient\Request;

use Going\Discount\ApiClient\Exception\DiscountApiRequestException;
use Going\Discount\ApiClient\Response\PricereductionBatchContainer;

interface PriceReductionRequestInterface
{
    /**
     * @return boolean
     */
    public function isSuccess();
    
    /**
     * @return string
     */
    public function getEndpoint();
    
    /**
     * @return string
     */
    public function getReqestType();
    
    /**
     * @return array
     */
    public function getResponse();
    
    /**
     * @return array
     */
    public function getPayload();
    
    /**
     * @param string $rawResponse
     * @return self
     */
    public function transformResponse($rawResponse);
    
    /**
     * @return string
     */
    public function getError();
    
    /**
     * @throws DiscountApiRequestException
     * @return PricereductionBatchContainer
     */
    public function getValues();
}