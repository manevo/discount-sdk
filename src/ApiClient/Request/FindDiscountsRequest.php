<?php

namespace Going\Discount\ApiClient\Request;

use Going\Discount\ApiClient\Exception\DiscountApiRequestException;
use Going\Discount\ApiClient\Request\Payload\Discount\FindFilteredDiscountsPayload;
use Going\Discount\ApiClient\Response\DiscountContainer;
use Going\Discount\ApiClient\Response\VO\DiscountDependencyResponseElement;
use Going\Discount\ApiClient\Response\VO\DiscountResponseElement;

class FindDiscountsRequest implements PriceReductionRequestInterface
{
    const BASE_RESPONSE_STRUCTURE =
        [
            'id',
            'type',
            'code',
            'value',
            'valuePercentage',
            'startDate',
            'expireDate',
            'dependencies',
            'itemLimit',
            'partnerId',
            'multiDiscount',
            'multiSize',
            'complimentary'
        ];
    const DEPENDENCIES_RESPONSE_STRUCTURE = ['type', 'externalId'];
    
    /**
     * @var string
     */
    protected $error = '';
    /**
     * @var DiscountContainer|null
     */
    protected $container;
    /**
     * @var bool
     */
    protected $success = false;
    /**
     * @var array
     */
    protected $response = [];
    
    /**
     * @var array
     */
    protected $meta = [];
    
    /**
     * @var array
     */
    protected $payload = [];
    
    /**
     * @var array
     */
    protected $parameters = [];
    
    
    const ENDPOINT = '/api/v1/discounts/code';
    
    /**
     * @param FindFilteredDiscountsPayload $payload
     */
    public function __construct(
        FindFilteredDiscountsPayload $payload
    ) {
        $this->parameters = $payload->jsonSerialize();
    }
    
    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }
    
    /**
     * @return string
     */
    public function getEndpoint()
    {
        return self::ENDPOINT.'?'.http_build_query($this->parameters, null, '&', PHP_QUERY_RFC3986);
    }
    
    /**
     * @return string
     */
    public function getReqestType()
    {
        return 'GET';
    }
    
    /**
     * @param string $rawResponse
     * @throws DiscountApiRequestException
     * @return self
     */
    public function transformResponse($rawResponse)
    {
        $response = \json_decode($rawResponse, true);
        
        if (empty($response) || !is_array($response) || !isset($response['data'])) {
            throw new DiscountApiRequestException('No valid api response ' . (string) $rawResponse);
        }
        
        if (isset($response['code'], $response['message']) && ($response['code'] === 404 || $response['code'] === 409)) {
            $this->success = false;
            $this->error = $response['message'];
            
            return $this;
        }
        
        $this->examineStructure($response['data']);
        
        if (isset($response['meta']['size'], $response['meta']['page'], $response['meta']['total'])) {
            $this->meta = $response['meta'];
        }
        
        $this->success = true;
        $this->response = $response['data'];
        
        return $this;
    }
    
    /**
     * @throws DiscountApiRequestException
     * @return void
     */
    private function examineStructure(array $responseData)
    {
        foreach ($responseData as $response) {
            foreach (self::BASE_RESPONSE_STRUCTURE as $element) {
                if (!\array_key_exists($element, $response)) {
                    throw new DiscountApiRequestException('No valid api response on main structure , missing : ' . $element);
                }
            }
            if (!empty($response['dependencies'])) {
                foreach ($response['dependencies'] as $dependency) {
                    foreach (self::DEPENDENCIES_RESPONSE_STRUCTURE as $item) {
                        if (!\array_key_exists($item, $dependency)) {
                            throw new DiscountApiRequestException('No valid api response on dependency structure , missing : ' . $item);
                        }
                    }
            
                }
            }
        }
    }
    
    /**
     * @return array
     */
    public function getResponse()
    {
        return $this->response;
    }
    
    /**
     * @return array
     */
    public function getPayload()
    {
        return $this->payload;
    }
    
    /**
     * @throws DiscountApiRequestException
     * @throws \Exception
     * @return DiscountContainer
     */
    public function getValues()
    {
        if ($this->container !== null) {
            return $this->container;
        }
        
        if (empty($this->response)) {
            throw new DiscountApiRequestException('Empty response');
        }
        $this->container = new DiscountContainer();
        
        foreach ($this->response as $element) {
            $result = new DiscountResponseElement(
                $element['id'],
                $element['type'],
                $element['code'],
                $element['value'],
                $element['valuePercentage'],
                !empty($element['startDate']) ? new \DateTime($element['startDate']) : null,
                !empty($element['expireDate']) ? new \DateTime($element['expireDate']) : null,
                !empty($element['dependencies']) && is_array($element['dependencies']) ? \array_map(static function ($element) {
                    return new DiscountDependencyResponseElement($element['type'], $element['externalId']);
                }, $element['dependencies']) : [],
                $element['itemLimit'],
                $element['partnerId'],
                $element['multiDiscount'],
                $element['multiSize'],
                (bool) $element['complimentary']
            );
            $this->container->addResult($result);
        }
        if (!empty($this->meta)) {
            $this->container->addMeta($this->meta['size'],$this->meta['page'],$this->meta['total']);
        }
        
        return $this->container;
    }
    
    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }
    
}