<?php

namespace Going\Discount\ApiClient\Request;

use Going\Discount\ApiClient\Exception\DiscountApiRequestException;

class DeleteDiscountRequest implements PriceReductionRequestInterface
{
    const ENDPOINT = '/api/v1/discount/%s/remove';
    
    /**
     * @var int
     */
    private $id;
    
    /**
     * @var string
     */
    protected $error = '';
    /**
     * @var bool
     */
    protected $success = false;
    /**
     * @var array
     */
    protected $response = [];
    
    /**
     * @var array
     */
    protected $payload = [];
    
    /**
     * @param int $id
     */
    public function __construct(
        $id
    ) {
        $this->id = $id;
    }
    
    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }
    
    /**
     * @return string
     */
    public function getEndpoint()
    {
        return sprintf(self::ENDPOINT, $this->id);
    }
    
    /**
     * @return string
     */
    public function getReqestType()
    {
        return 'GET';
    }
    
    /**
     * @param string $rawResponse
     * @throws DiscountApiRequestException
     * @return self
     */
    public function transformResponse($rawResponse)
    {
        $response = \json_decode($rawResponse, true);
        
        if (empty($response) || !is_array($response) || !isset($response['data'])) {
            throw new DiscountApiRequestException('No valid api response ' . (string) $rawResponse);
        }
        
        if (isset($response['code'], $response['message']) && ($response['code'] === 404 || $response['code'] === 409)) {
            $this->success = false;
            $this->error = $response['message'];
            
            return $this;
        }
        
        $this->success = true;
        $this->response = $response['data'];
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getResponse()
    {
        return $this->response;
    }
    
    /**
     * @return array
     */
    public function getPayload()
    {
        return $this->payload;
    }
    
    /**
     * @return null
     */
    public function getValues()
    {
        return null;
    }
    
    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }
}