<?php

namespace Going\Discount\ApiClient\Request;

use Going\Discount\ApiClient\Exception\DiscountApiRequestException;
use Going\Discount\ApiClient\Request\Payload\PriceReductionPayload;
use Going\Discount\ApiClient\Response\PricereductionBatchContainer;
use Going\Discount\ApiClient\Response\VO\PriceResponseElement;

class PriceReductionRequest implements PriceReductionRequestInterface
{
    const BASE_RESPONSE_STRUCTURE = ['tickets', 'reservations', 'products'];
    const VALUE_RESPONSE_STRUCTURE = ['amountTotal', 'quantity', 'id', 'amountReduced', 'positionNumber'];
    const RESERVATIONS_RESPONSE_STRUCTURE = 'products';
    const RESERVATIONS_STRUCTURE = 'reservations';
    
    const ENDPOINT = '/api/v1/pricereduction/%s';
    /**
     * @var string
     */
    protected $error = '';
    /**
     * @var PricereductionBatchContainer|null
     */
    private $container;
    /**
     * @var bool
     */
    private $success = false;
    /**
     * @var array
     */
    private $response = [];
    
    /**
     * @var array
     */
    private $payload = [];
    
    /**
     * @var string
     */
    private $type;
    
    /**
     * @param PriceReductionPayload $payload
     */
    public function __construct(
        PriceReductionPayload $payload
    ) {
        $this->type = $payload->type;
        $this->payload = $payload->jsonSerialize();
    }
    
    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }
    
    /**
     * @return string
     */
    public function getEndpoint()
    {
        return \sprintf(self::ENDPOINT, $this->type);
    }
    
    /**
     * @return string
     */
    public function getReqestType()
    {
        return 'POST';
    }
    
    /**
     * @param string $rawResponse
     * @throws DiscountApiRequestException
     * @return self
     */
    public function transformResponse($rawResponse)
    {
        $response = \json_decode($rawResponse, true);
        
        if (empty($response) || !is_array($response) || !isset($response['data'])) {
            throw new DiscountApiRequestException('No valid api response ' . (string) $rawResponse);
        }
        
        if (isset($response['code'], $response['message']) && ($response['code'] === 404 || $response['code'] === 409)) {
            $this->success = false;
            $this->error = $response['message'];
            
            return $this;
        }
        
        if (empty($response['data'][0])) {
            throw new DiscountApiRequestException('Empty api response ' . (string) $rawResponse);
        }
        
        $this->examineStructure($response['data'][0]);
        $this->success = true;
        $this->response = $response['data'][0];
        
        return $this;
    }
    
    /**
     * @throws DiscountApiRequestException
     * @return void
     */
    private function examineStructure(array $response)
    {
        foreach (self::BASE_RESPONSE_STRUCTURE as $element) {
            if (!\array_key_exists($element, $response)) {
                throw new DiscountApiRequestException('No valid api response on main structure , missing : ' . $element);
            }
        }
        
        foreach (self::BASE_RESPONSE_STRUCTURE as $index) {
            foreach ($response[$index] as $key => $value) {
                foreach (self::VALUE_RESPONSE_STRUCTURE as $element) {
                    if (!\array_key_exists($element, $value)) {
                        var_dump($value);
                        throw new DiscountApiRequestException('No valid api response on value , missing : ' . $element);
                    }
            
                    if ($key === self::RESERVATIONS_STRUCTURE && isset($value[self::RESERVATIONS_RESPONSE_STRUCTURE])) {
                        foreach (self::VALUE_RESPONSE_STRUCTURE as $sourceElement) {
                            if (!\array_key_exists($sourceElement, $value[self::RESERVATIONS_RESPONSE_STRUCTURE])) {
                                throw new DiscountApiRequestException('No valid api response on value , missing : ' . $element);
                            }
                        }
                    }
                }
            }
        }
    }
    
    /**
     * @return array
     */
    public function getResponse()
    {
        return $this->response;
    }
    
    /**
     * @return array
     */
    public function getPayload()
    {
        return $this->payload;
    }
    
    /**
     * @throws DiscountApiRequestException
     * @return PricereductionBatchContainer
     */
    public function getValues()
    {
        if ($this->container !== null) {
            return $this->container;
        }
        
        if (empty($this->response)) {
            throw new DiscountApiRequestException('Empty response');
        }
        $this->container = new PricereductionBatchContainer();
        
        foreach ($this->response as $key => $types) {
            foreach ($types as $elements) {
                $data = new PriceResponseElement(
                    $key,
                    (int) $elements['amountTotal'],
                    (int) $elements['quantity'],
                    (int) $elements['id'],
                    (int) $elements['amountReduced'],
                    (int) $elements['positionNumber']
                );
                    
                    if ($key === 'reservations' && !empty($elements['products'])) {
                        foreach ($elements['products'] as $productElement) {
                            $dataProduct = new PriceResponseElement(
                                'products',
                                (int) $productElement['amountTotal'],
                                (int) $productElement['quantity'],
                                (int) $productElement['id'],
                                (int) $productElement['amountReduced'],
                                (int) $productElement['positionNumber']
                            );
                            $this->container->addValue($dataProduct);
                        }
                    }
            $this->container->addValue($data);
            }
            
        }
        
        return $this->container;
    }
    
    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }
}