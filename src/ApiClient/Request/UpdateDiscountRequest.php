<?php

namespace Going\Discount\ApiClient\Request;

use Going\Discount\ApiClient\Request\Payload\Discount\UpdateDiscountPayload;

class UpdateDiscountRequest extends CrudDiscountRequest implements PriceReductionRequestInterface
{
    const ENDPOINT = '/api/v1/discount/update/%s';
    
    /**
     * @var int
     */
    private $id;
    
    /**
     * @param UpdateDiscountPayload $payload
     */
    public function __construct(
        UpdateDiscountPayload $payload
    ) {
        $this->payload = $payload->jsonSerialize();
        $this->id = $payload->id;
    }
    
    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }
    
    /**
     * @return string
     */
    public function getEndpoint()
    {
        return sprintf(self::ENDPOINT, $this->id);
    }
    
    /**
     * @return string
     */
    public function getReqestType()
    {
        return 'POST';
    }
}