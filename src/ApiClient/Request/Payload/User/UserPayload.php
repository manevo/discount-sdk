<?php

namespace Going\Discount\ApiClient\Request\Payload\User;

class UserPayload implements \JsonSerializable
{
    /**
     * @var string
     */
    public $email;
    
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}