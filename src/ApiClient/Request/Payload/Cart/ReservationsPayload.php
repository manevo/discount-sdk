<?php

namespace Going\Discount\ApiClient\Request\Payload\Cart;

use JsonSerializable;

class ReservationsPayload extends PositionPayload implements \JsonSerializable, CartPositionInterface
{
    /**
     * @var ProductsPayload[]|array
     */
    public $products = [];
    
    public function jsonSerialize()
    {
        $result = parent::jsonSerialize();
        $result['products'] = \array_map(static function (JsonSerializable $element) { return $element->jsonSerialize();}, $this->products);
        
        return $result;
    }
    
}