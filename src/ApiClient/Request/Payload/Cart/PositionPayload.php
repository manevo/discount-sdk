<?php

namespace Going\Discount\ApiClient\Request\Payload\Cart;

use \JsonSerializable;

abstract class PositionPayload implements \JsonSerializable
{
    /**
     * @var int
     */
    public $amountTotal;
    /**
     * @var int
     */
    public $quantity;
    /**
     * @var int
     */
    public $positionNumber;
    
    /**
     * @var DependencyPayload[]|array
     */
    public $dependencies;
    
    /**
     * @param string $type
     * @param int $externalId
     * @return $this
     */
    public function addDependency($type, $externalId)
    {
        if (!empty($type)) {
            $this->dependencies[] = new DependencyPayload((string) $type, (int) $externalId);
        }
        
        return $this;
    }
    
    public function jsonSerialize()
    {
        return [
            'amountTotal' => $this->amountTotal,
            'quantity' => $this->quantity,
            'positionNumber' =>  $this->positionNumber,
            'dependencies' => \array_map(static function (JsonSerializable $element) { return $element->jsonSerialize();}, $this->dependencies)
        ];
    }
}