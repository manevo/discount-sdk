<?php

namespace Going\Discount\ApiClient\Request\Payload\Cart;

use Going\Discount\Util\JsonTrait;

class CartPayload implements \JsonSerializable
{
    use JsonTrait;
    
    /**
     * @var TicketsPayload[]
     */
    public  $tickets;
    
    /**
     * @var ReservationsPayload[]
     */
    public $reservations;
    
    /**
     * @var ProductsPayload[]
     */
    public $products;
    
    public function jsonSerialize()
    {
        return  [
          'tickets' => \array_map('\Going\Discount\Util\JsonTrait::toJson', $this->tickets),
          'reservations' => \array_map('\Going\Discount\Util\JsonTrait::toJson', $this->reservations),
          'products' => \array_map('\Going\Discount\Util\JsonTrait::toJson', $this->products),
        ];
    }
}