<?php

namespace Going\Discount\ApiClient\Request\Payload\Cart;

class DependencyPayload implements \JsonSerializable
{
    /**
     * @var string
     */
    public $type;
    /**
     * @var int
     */
    public $externalId;
    
    /**
     * @param string $type
     * @param int $externalId
     */
    public function __construct($type, $externalId)
    {
        $this->type = $type;
        $this->externalId = $externalId;
    }
    
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}