<?php

namespace Going\Discount\ApiClient\Request\Payload\Discount;

class DependencyDiscountPayload implements \JsonSerializable
{
    /**
     * @example pool, rundate, user, partner
     * @var string
     */
    public $type;
    /**
     * @var int[]
     */
    public $ids = [];
    
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}