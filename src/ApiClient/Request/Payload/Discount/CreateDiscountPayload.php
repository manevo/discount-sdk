<?php

namespace Going\Discount\ApiClient\Request\Payload\Discount;

use Going\Discount\Util\JsonTrait;

class CreateDiscountPayload implements \JsonSerializable
{
    use JsonTrait;
    
    /**
     * @var string
     */
    public $code;
    
    /**
     * at now default
     * @var string
     */
    public $type = 'discount';
    
    /**
     * @var integer|null
     */
    public $value;
    
    /**
     * @var integer|null
     */
    public $valuePercentage;
    
    /**
     * @var string|null
     */
    public $startDate;
    
    /**
     * @var string|null
     */
    public $expireDate;
    
    /**
     * @var int|null
     */
    public $ticketLimit;
    
    /**
     * @var int|null
     */
    public $partnerId;
    
    /**
     * @var int|null
     */
    public $multiDiscount;
    
    /**
     * @var int|null
     */
    public $multiSize;
    
    /**
     * @var int|null
     */
    public $itemLimit;
    
    /**
     * @var boolean
     */
    public $complimentary = false;
    
    /**
     * @var DependencyDiscountPayload[]
     */
    public $dependencies = [];
    
    
    public function jsonSerialize()
    {
        $result = get_object_vars($this);
        $result['dependencies'] = \array_map('\Going\Discount\Util\JsonTrait::toJson', $this->dependencies);
        
        return $result;
    }
}