<?php

namespace Going\Discount\ApiClient\Request\Payload\Discount;

class FindFilteredDiscountsPayload implements \JsonSerializable
{
    /**
     * @var int
     */
    private $page;
    /**
     * @var int
     */
    private $count;
    /**
     * @var string|null
     */
    private $code;
    /**
     * @var int|null
     */
    private $partnerId;
    
    /**
     * @param int $page
     * @param int $count
     * @param string|null $code
     * @param int|null $partnerId
     */
    public function __construct($page, $count, $code, $partnerId)
    {
        $this->page = $page;
        $this->count = $count;
        $this->code = $code;
        $this->partnerId = $partnerId;
    }
    
    
    public function jsonSerialize()
    {
        return array_filter(get_object_vars($this), static function ($value) { return $value !== null;});
    }
}