<?php

namespace Going\Discount\ApiClient\Request\Payload;

use Going\Discount\ApiClient\Request\Payload\Cart\CartPayload;
use Going\Discount\ApiClient\Request\Payload\User\UserPayload;

class PriceReductionPayload implements \JsonSerializable
{
    /**
     * @var string
     */
    public $type;
    
    /**
     * @var string
     */
    public $code;
    
    /**
     * @var CartPayload
     */
    public $cart;
    
    /**
     * @var UserPayload|null
     */
    public $user;
    
    /**
     * @param string $type
     */
    public function __construct($type)
    {
        $this->type = $type;
    }
    
    
    public function jsonSerialize()
    {
        return [
            'code' => $this->code,
            'cart' => $this->cart->jsonSerialize(),
            'user' =>  $this->user !== null ? $this->user->jsonSerialize() : null
        ];
    }
}