<?php

namespace Going\Discount\ApiClient;

use Going\Discount\ApiClient\Request\Payload\Cart\CartPayload;
use Going\Discount\ApiClient\Request\Payload\Cart\CartPositionInterface;
use Going\Discount\ApiClient\Request\Payload\Cart\DependencyPayload;
use Going\Discount\ApiClient\Request\Payload\Cart\ProductsPayload;
use Going\Discount\ApiClient\Request\Payload\Cart\ReservationsPayload;
use Going\Discount\ApiClient\Request\Payload\Cart\TicketsPayload;
use Going\Discount\ApiClient\Request\Payload\User\UserPayload;
use Going\Discount\ApiClient\Request\Payload\PriceReductionPayload;

class PriceReductionPayloadBuilder
{
    /**
     * @example discount,prepaidcard
     * @var string
     */
    private $type;
    
    /**
     * @var array
     */
    private $positions = [];
    
    /**
     * @var int
     */
    private $positionNo = 0;
    
    private $reservationBatches = [];
    private $ticketsBatches = [];
    private $productsBatches = [];
    
    /**
     * @param string $type
     */
    public function __construct($type)
    {
        $this->type = $type;
    }
    
    /**
     * @return PriceReductionPayloadBuilder
     */
    public static function makeDiscountPayload()
    {
        return new self('discount');
    }
    
    /**
     * @return PriceReductionPayloadBuilder
     */
    public static function makePrepaidPayload()
    {
        return new self('prepaidcard');
    }
    
    /**
     * @param int|null $positionNo
     * @param DependencyPayload[]|array $dependencies
     * @param int $amountTotal
     * @param int $quantity
     *
     * @return TicketsPayload
     */
    public function makeTicketPosition(
        $positionNo,
        $amountTotal,
        $quantity,
        array $dependencies = []
    ) {
        $batch = new TicketsPayload;
    
        $this->assemblePayload($dependencies, $batch, $positionNo, $quantity, $amountTotal);
    
    
        $this->ticketsBatches[] = $batch;
        
        return $batch;
    }
    
    /**
     * @param int|null $positionNo
     * @param DependencyPayload[]|array $dependencies
     * @param int $amountTotal
     * @param int $quantity
     *
     * @return ProductsPayload
     */
    public function makeProductsPosition(
        $positionNo,
        $amountTotal,
        $quantity,
        array $dependencies = []
    ) {
        $batch = new ProductsPayload();
        
        $this->assemblePayload($dependencies, $batch, $positionNo, $quantity, $amountTotal);
        
        
        $this->productsBatches[] = $batch;
        
        return $batch;
    }
    
    /**
     * @param ReservationsPayload $reservation
     * @param int|null $positionNo
     * @param DependencyPayload[]|array $dependencies
     * @param int $amountTotal
     * @param int $quantity
     *
     * @return ReservationsPayload
     */
    public function addReservationProductsPosition(
        $reservation,
        $positionNo,
        $amountTotal,
        $quantity,
        array $dependencies = []
    ) {
        $batch = new ProductsPayload();
        
        $this->assemblePayload($dependencies, $batch, $positionNo, $quantity, $amountTotal);
        
        $reservation->products[] = $batch;
        
        return $reservation;
    }
    
    /**
     * @param int|null $positionNo
     * @param DependencyPayload[]|array $dependencies
     * @param int $amountTotal
     * @param int $quantity
     *
     * @return ReservationsPayload
     */
    public function makeReservationsPosition(
        $positionNo,
        $amountTotal,
        $quantity,
        array $dependencies = []
    ) {
        $batch = new ReservationsPayload();
        
        $this->assemblePayload($dependencies, $batch, $positionNo, $quantity, $amountTotal);
        
        
        $this->productsBatches[] = $batch;
        
        return $batch;
    }
    
    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * @param string $code
     * @param UserPayload|null $user
     * @return PriceReductionPayload
     */
    public function build($code, $user = null)
    {
        if (empty($this->ticketsBatches) && empty($this->productsBatches) && empty($this->reservationBatches)) {
            throw new \UnexpectedValueException('Cannot build without batches');
        }
        
        $cart = new CartPayload();
        $cart->tickets = $this->ticketsBatches;
        $cart->products = $this->productsBatches;
        $cart->reservations = $this->reservationBatches;
        
        $payload = new PriceReductionPayload($this->type);
        $payload->user = $user;
        $payload->code = $code;
        $payload->cart = $cart;
        
        return $payload;
    }
    
    /**
     * @param array $dependencies
     * @return bool
     */
    private function assertCanProcessDependencies(array $dependencies)
    {
        return \array_walk($dependencies, static function ($element) {
            if (!($element instanceof DependencyPayload)) {
                throw new \UnexpectedValueException('This should be DependencyPayload class');
            }
        });
    }
    
    /**
     * @param array $dependencies
     * @param CartPositionInterface $batch
     * @param int|null$positionNo
     * @param int $quantity
     * @param int $amountTotal
     */
    private function assemblePayload(array $dependencies, CartPositionInterface $batch, $positionNo, $quantity, $amountTotal)
    {
        if (!empty($dependencies) && $this->assertCanProcessDependencies($dependencies)) {
            $batch->dependencies = $dependencies;
        }
        
        if ($positionNo === null) {
            do {
                ++$this->positionNo;
                $this->positions[] = $this->positionNo;
                
            } while (!in_array($this->positionNo, $this->positions, true));
        }
        
        $batch->positionNumber = $this->positionNo;
        $batch->quantity = (int) $quantity;
        $batch->amountTotal = (int) $amountTotal;
    }
}