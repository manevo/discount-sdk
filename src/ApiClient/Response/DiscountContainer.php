<?php

namespace Going\Discount\ApiClient\Response;

use Going\Discount\ApiClient\Response\VO\DiscountResponseElement;

class DiscountContainer
{
    /**
     * @var array|DiscountResponseElement[]
     */
    private $results = [];
    
    /**
     * @var int|null
     */
    private $size;
    /**
     * @var int|null
     */
    private $page;
    /**
     * @var int|null
     */
    private $total;
    
    /**
     * @return void
     */
    public function addResult(DiscountResponseElement $element)
    {
        $this->results[] = $element;
    }
    
    /**
     * @param int $size
     * @param int $page
     * @param int $total
     * @return void
     */
    public function addMeta($size, $page, $total)
    {
        $this->size = $size;
        $this->page = $page;
        $this->total = $total;
    }
    
    /**
     * @return array|DiscountResponseElement[]
     */
    public function getResults()
    {
        return $this->results;
    }
    
    /**
     * @return DiscountResponseElement|null
     */
    public function getSingleResult()
    {
        return isset($this->results[0]) ? $this->results[0] : null ;
    }
    
    /**
     * @return int|null
     */
    public function getSize()
    {
        return $this->size;
    }
    
    /**
     * @return int|null
     */
    public function getPage()
    {
        return $this->page;
    }
    
    /**
     * @return int|null
     */
    public function getTotal()
    {
        return $this->total;
    }
}