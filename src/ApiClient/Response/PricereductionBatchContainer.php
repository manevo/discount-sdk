<?php

namespace Going\Discount\ApiClient\Response;

use Going\Discount\ApiClient\Response\VO\PriceResponseElement;

class PricereductionBatchContainer
{
    /**
     * @var array
     */
    private $values;

    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param PriceResponseElement $value
     * @return void
     */
    public function addValue(PriceResponseElement $value)
    {
        $this->values[$value->getPositionNumber()] = $value;
    }


    /**
     * @param int
     * @return PriceResponseElement|null
     */
    public function getValue($positionNumber)
    {
        return isset($this->values[$positionNumber]) ? $this->values[$positionNumber] : null;
    }

    /**
     * @param int
     * @return bool
     */
    public function hasValue($positionNumber)
    {
        return isset($this->values[$positionNumber]);
    }
}
