<?php

namespace Going\Discount\ApiClient\Response\VO;

class PriceResponseElement
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var integer
     */
    private $amountTotal;
    /**
     * @var integer
     */
    private $quantity;
    /**
     * @var integer
     */
    private $id;
    /**
     * @var integer
     */
    private $amountReduced;
    /**
     * @var integer
     */
    private $positionNumber;

    /**
     * @param string $type
     * @param int $amountTotal
     * @param int $quantity
     * @param int $id
     * @param int $amountReduced
     * @param int $positionNumber
     */
    public function __construct($type, $amountTotal, $quantity, $id, $amountReduced, $positionNumber)
    {
        $this->type = $type;
        $this->amountTotal = $amountTotal;
        $this->quantity = $quantity;
        $this->id = $id;
        $this->amountReduced = $amountReduced;
        $this->positionNumber = $positionNumber;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getAmountTotal()
    {
        return $this->amountTotal;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAmountReduced()
    {
        return $this->amountReduced;
    }

    /**
     * @return int
     */
    public function getPositionNumber()
    {
        return $this->positionNumber;
    }

    /**
     * @return boolean
     */
    public function wasReduced()
    {
        return $this->amountReduced !== 0;
    }

    /**
     * @return int
     */
    public function getUnitAMount()
    {
        return (int) ($this->amountTotal/$this->quantity);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'amount' => $this->amountTotal,
            'amountReduced' => $this->amountReduced,
            'quantity' => $this->quantity,
            'type' => $this->type
        ];
    }
}
