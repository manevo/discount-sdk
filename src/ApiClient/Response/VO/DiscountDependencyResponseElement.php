<?php

namespace Going\Discount\ApiClient\Response\VO;

class DiscountDependencyResponseElement
{
    /**
     * @var string
     */
    private $type;
    /**
     * @var int
     */
    private $externalId;
    
    /**
     * @param string $type
     * @param int $externalId
     */
    public function __construct($type, $externalId)
    {
        $this->type = $type;
        $this->externalId = $externalId;
    }
    
    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * @return int
     */
    public function getExternalId()
    {
        return $this->externalId;
    }
}