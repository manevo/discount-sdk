<?php

namespace Going\Discount\ApiClient\Response\VO;

class DiscountResponseElement
{
    /**
     * @var int|int
     */
    private $id;
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $code;
    /**
     * @var int|null
     */
    private $value;
    /**
     * @var int|null
     */
    private $valuePercentage;
    /**
     * @var \DateTime|null
     */
    private $startDate;
    /**
     * @var \DateTime|null
     */
    private $expireDate;
    /**
     * @var array
     */
    private $dependencies;
    /**
     * @var int|null
     */
    private $itemLimit;
    /**
     * @var int|null
     */
    private $partnerId;
    /**
     * @var int|null
     */
    private $multiDiscount;
    /**
     * @var int|null
     */
    private $multiSize;
    /**
     * @var bool|null
     */
    private $complimentary;
    
    /**
     * @param int $id
     * @param string $type
     * @param string $code
     * @param int|null $value
     * @param int|null $valuePercentage
     * @param \DateTime|null $startDate
     * @param \DateTime|null $expireDate
     * @param array $dependencies
     * @param int|null $itemLimit
     * @param int|null $partnerId
     * @param int|null $multiDiscount
     * @param int|null $multiSize
     * @param bool|null $complimentary
     */
    public function __construct(
        $id,
        $type,
        $code,
        $value,
        $valuePercentage,
        $startDate,
        $expireDate,
        array $dependencies,
        $itemLimit,
        $partnerId,
        $multiDiscount,
        $multiSize,
        $complimentary
    ) {
        $this->id = $id;
        $this->type = $type;
        $this->code = $code;
        $this->value = $value;
        $this->valuePercentage = $valuePercentage;
        $this->startDate = $startDate;
        $this->expireDate = $expireDate;
        $this->dependencies = $dependencies;
        $this->itemLimit = $itemLimit;
        $this->partnerId = $partnerId;
        $this->multiDiscount = $multiDiscount;
        $this->multiSize = $multiSize;
        $this->complimentary = $complimentary;
    }
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * @return int|null
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * @return int|null
     */
    public function getValuePercentage()
    {
        return $this->valuePercentage;
    }
    
    /**
     * @return \DateTime|null
     */
    public function getStartDate()
    {
        return $this->startDate;
    }
    
    /**
     * @return \DateTime|null
     */
    public function getExpireDate()
    {
        return $this->expireDate;
    }
    
    /**
     * @return array
     */
    public function getDependencies()
    {
        return $this->dependencies;
    }
    
    /**
     * @return int|null
     */
    public function getItemLimit()
    {
        return $this->itemLimit;
    }
    
    /**
     * @return int|null
     */
    public function getPartnerId()
    {
        return $this->partnerId;
    }
    
    /**
     * @return int|null
     */
    public function getMultiDiscount()
    {
        return $this->multiDiscount;
    }
    
    /**
     * @return int|null
     */
    public function getMultiSize()
    {
        return $this->multiSize;
    }
    
    /**
     * @return bool|null
     */
    public function getComplimentary()
    {
        return $this->complimentary;
    }
}