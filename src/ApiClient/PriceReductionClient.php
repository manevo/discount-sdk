<?php

namespace Going\Discount\ApiClient;

use Going\Discount\ApiClient\Request\CreateDiscountRequest;
use Going\Discount\ApiClient\Request\DeleteDiscountRequest;
use Going\Discount\ApiClient\Request\GenerateDiscountsRequest;
use Going\Discount\ApiClient\Request\Payload\Discount\CreateDiscountPayload;
use Going\Discount\ApiClient\Request\Payload\Discount\GenerateDiscountPayload;
use Going\Discount\ApiClient\Request\Payload\Discount\UpdateDiscountPayload;
use Going\Discount\ApiClient\Request\Payload\PriceReductionPayload;
use Going\Discount\ApiClient\Request\PriceReductionRequest;
use Going\Discount\ApiClient\Request\PriceReductionRequestInterface;
use Going\Discount\ApiClient\Request\UpdateDiscountRequest;
use GuzzleHttp\Exception\GuzzleException;

class PriceReductionClient extends ApiClient
{
    /**
     * @param PriceReductionPayload $payload
     * @throws Exception\DiscountApiRequestException
     * @throws GuzzleException
     * @return PriceReductionRequestInterface
     */
    public function callReducePrice(PriceReductionPayload $payload)
    {
        return $this->request(new PriceReductionRequest($payload));
    }
    
    /**
     * @param CreateDiscountPayload $payload
     * @throws Exception\DiscountApiRequestException
     * @throws GuzzleException
     * @return PriceReductionRequestInterface
     */
    public function callCreateDiscount(CreateDiscountPayload $payload)
    {
        return $this->request(new CreateDiscountRequest($payload));
    }
    
    /**
     * @param GenerateDiscountPayload $payload
     * @throws Exception\DiscountApiRequestException
     * @throws GuzzleException
     * @return PriceReductionRequestInterface
     */
    public function callGenerateDiscount(GenerateDiscountPayload $payload)
    {
        return $this->request(new GenerateDiscountsRequest($payload));
    }
    
    /**
     * @param UpdateDiscountPayload $payload
     * @throws Exception\DiscountApiRequestException
     * @throws GuzzleException
     * @return PriceReductionRequestInterface
     */
    public function callUpdateDiscount(UpdateDiscountPayload $payload)
    {
        return $this->request(new UpdateDiscountRequest($payload));
    }
    
    /**
     * @param integer $id
     * @throws Exception\DiscountApiRequestException
     * @throws GuzzleException
     * @return PriceReductionRequestInterface
     */
    public function callDeleteDiscount($id)
    {
        return $this->request(new DeleteDiscountRequest($id));
    }
}