<?php

namespace Going\Discount\ApiClient;

use Going\Discount\ApiClient\Request\Payload\Discount\CreateDiscountPayload;
use Going\Discount\ApiClient\Request\Payload\Discount\DependencyDiscountPayload;
use Going\Discount\ApiClient\Request\Payload\Discount\FindFilteredDiscountsPayload;
use Going\Discount\ApiClient\Request\Payload\Discount\GenerateDiscountPayload;
use Going\Discount\ApiClient\Request\Payload\Discount\UpdateDiscountPayload;

class DiscountPayloadBuilder
{
    
    /**
     * @param int $page
     * @param int $count
     * @param null|string $code
     * @param null|int $partnerId
     * @return FindFilteredDiscountsPayload
     */
    public function makeGetDiscountsFilteredPayload(
        $page = 1,
        $count = 20,
        $code = null,
        $partnerId = null
    ) {
        return new FindFilteredDiscountsPayload($page, $count, $code,  $partnerId);
    }
    
    
    /**
     * @param int $quantity
     * @param string $prefix
     * @param int|null $value
     * @param int|null $valuePercentage
     * @param \DateTime|null $startDate
     * @param \DateTime|null $expireDate
     * @param int|null $partnerId
     * @param int|null $multiDiscount
     * @param int|null $multiSize
     * @param int|null $itemLimit
     * @param boolean $complimentary
     * @return GenerateDiscountPayload
     */
    public function makeGenerateDiscountPayload(
        $quantity,
        $prefix = '',
        $value = null,
        $valuePercentage = null,
        $startDate = null,
        $expireDate = null,
        $partnerId = null,
        $multiDiscount = null,
        $multiSize = null,
        $itemLimit = null,
        $complimentary = false
    ) {
        
        if ($quantity<= 0) {
            throw new \UnexpectedValueException(' Quantity shoud be greather than 0');
        }
        $result = new GenerateDiscountPayload();
        $result->quantity = $quantity;
        $result->prefix = $prefix;
        $result->value = $value;
        $result->valuePercentage = $valuePercentage;
        $result->startDate = $startDate instanceof \DateTime ? $startDate->format(DATE_ATOM) : null;
        $result->expireDate = $expireDate instanceof \DateTime ? $expireDate->format(DATE_ATOM) : null;
        $result->partnerId = $partnerId;
        $result->multiDiscount = $multiDiscount;
        $result->multiSize = $multiSize;
        $result->itemLimit = $itemLimit;
        $result->complimentary = $complimentary;
        
        return $result;
    }
    
    
    
    /**
     * @param string $code
     * @param int|null $value
     * @param int|null $valuePercentage
     * @param \DateTime|null $startDate
     * @param \DateTime|null $expireDate
     * @param int|null $partnerId
     * @param int|null $multiDiscount
     * @param int|null $multiSize
     * @param int|null $itemLimit
     * @param boolean $complimentary
     * @return CreateDiscountPayload
     */
    public function makeCreateDiscountPayload(
        $code,
        $value = null,
        $valuePercentage = null,
        $startDate = null,
        $expireDate = null,
        $partnerId = null,
        $multiDiscount = null,
        $multiSize = null,
        $itemLimit = null,
        $complimentary = false
    ) {
        $result = new CreateDiscountPayload();
        $result->code = $code;
        $result->value = $value;
        $result->valuePercentage = $valuePercentage;
        $result->startDate = $startDate instanceof \DateTime ? $startDate->format(DATE_ATOM) : null;
        $result->expireDate = $expireDate instanceof \DateTime ? $expireDate->format(DATE_ATOM) : null;
        $result->partnerId = $partnerId;
        $result->multiDiscount = $multiDiscount;
        $result->multiSize = $multiSize;
        $result->itemLimit = $itemLimit;
        $result->complimentary = $complimentary;
        
        return $result;
    }
    
    /**
     * @param int $id
     * @param string $code
     * @param int|null $value
     * @param int|null $valuePercentage
     * @param \DateTime|null $startDate
     * @param \DateTime|null $expireDate
     * @param int|null $partnerId
     * @param int|null $multiDiscount
     * @param int|null $multiSize
     * @param int|null $itemLimit
     * @param boolean $complimentary
     * @return UpdateDiscountPayload
     */
    public function makeUpdateDiscountPayload(
        $id,
        $code,
        $value = null,
        $valuePercentage = null,
        $startDate = null,
        $expireDate = null,
        $partnerId = null,
        $multiDiscount = null,
        $multiSize = null,
        $itemLimit = null,
        $complimentary = false
    ) {
        $result = new UpdateDiscountPayload();
        $result->id = $id;
        $result->code = $code;
        $result->value = $value;
        $result->valuePercentage = $valuePercentage;
        $result->startDate = $startDate instanceof \DateTime ? $startDate->format(DATE_ATOM) : null;
        $result->expireDate = $expireDate instanceof \DateTime ? $expireDate->format(DATE_ATOM) : null;
        $result->partnerId = $partnerId;
        $result->multiDiscount = $multiDiscount;
        $result->multiSize = $multiSize;
        $result->itemLimit = $itemLimit;
        $result->complimentary = $complimentary;
        
        return $result;
    }
    
    
    /**
     * @param CreateDiscountPayload|UpdateDiscountPayload|GenerateDiscountPayload $payload
     * @param string $type
     * @param array $ids
     */
    public function addDiscountPayloadDependency($payload, $type, array $ids)
    {
        if (empty($type) || empty($ids)) {
            throw new \UnexpectedValueException('Dependency values cannot be empty');
        }
        
        $data = new DependencyDiscountPayload();
        $data->type = (string) $type;
        $data->ids = \array_map('intval', $ids);
        
        $payload->dependencies[] = $data;
    }
}