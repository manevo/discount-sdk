## **DISCOUNT  SDK**

## Table of Contents

- [Introduction](#introduction)
- [API](#api)
- [Service Dependencies](#service-dependencies)
- [Built with](#built-with)
- [Features](#features)
- [Quick Start](#quick-start)
- [Testing](#testing)


## Introduction
SDK do serwisu ms Discount

## API
brak

## Service Dependencies
Guzzle

## Built with
guzzle

## Features
- crud Discount (get, create, delete, update)
- pozyskanie redukcji ceny (walidacja discount , price reduction)


## Quick Start

```
$client = new PriceReductionClient('https://api','key')
```
Budowanie requestu eg Create:

        $builder = new DiscountPayloadBuilder();
        $payload = $builder->makeCreateDiscountPayload(
            'test'
        );
        $builder->addDiscountPayloadDependency($payload, 'pool' , [1,2]);

Request:

```
$result = $client->callCreateDiscount($payload);
//obiekt discount
$discount = $result->getSingleResult();
```

Pytanie o discount podając pozycje koszyka z numeracją ich (walidacja)

       $builder = PriceReductionPayloadBuilder::makeDiscountPayload();
        $builder->makeTicketPosition(
            4,
            10000,
            1,
            [
                new DependencyPayload('pool',2),
                new DependencyPayload('event',2),
                new DependencyPayload('rundate',2)
            ]
        );
        
        $reservation = $builder->makeReservationsPosition(
            1,
            10000,
            4,
            [
                new DependencyPayload('pool',1),
                new DependencyPayload('event',1),
                new DependencyPayload('rundate',1)
            ]
        );
        
        $builder->addReservationProductsPosition(
            $reservation,
            2,
            35000,
            1,
            [ new DependencyPayload('product',1)]
        );
        
        return $builder->build('test11', new UserPayload('marek@zzzzzzzzzzzzzz.wp') );
        

Request 

```
$result = $client->callReducePrice($payload);
// kontener pozycji i ich rezultatów
$container = $result->getValues();

$position = $container->getValue(4);
// ile zredukowano cene na pozycji 4
$position->getAmountReduced();
```

## Testing
php vendor/phpunit/phpunit/phpunit tests


